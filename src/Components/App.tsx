import { Circle } from 'rc-progress'
import React, { useEffect, useRef } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import { ReactComponent as PauseButton } from '../icons/pause-solid.svg'
import { ReactComponent as PlayButton } from '../icons/play-solid.svg'
import { ReactComponent as ResetButton } from '../icons/redo-alt-solid.svg'
import {
   BREAK,
   COUNTER_TYPE,
   decrement,
   getBreakLength,
   getIsPlaying,
   getPercentageCompleted,
   getSessionLength,
   getTimeRemaining,
   getTimerType,
   increment,
   resetCounter,
   SESSION,
   togglePlayPause,
   updateTimer
} from '../redux/breakReducer'
import { millisToMinutesAndSeconds, minutesToMilliseconds, noop } from '../utils'
import { TimerInput } from './TimerInput'

const App: React.FC = () => {
   const audioFile = useRef<HTMLAudioElement>(null)
   const dispatch = useDispatch()

   const breakLength = useSelector(getBreakLength)
   const sessionLength = useSelector(getSessionLength)
   const timeRemaining = useSelector(getTimeRemaining)
   const timerType = useSelector(getTimerType)
   const isPlaying = useSelector(getIsPlaying)
   const percentageCompleted = useSelector(getPercentageCompleted)

   const counterTime = millisToMinutesAndSeconds(timeRemaining)

   let timerColor = timerType === SESSION ? '#3D9970' : '#0074D9'

   if (timerType === SESSION) {
      if (timeRemaining <= minutesToMilliseconds(sessionLength) / 2) {
         timerColor = '#FFDC00'
      }
      if (timeRemaining <= 60000) timerColor = '#85144b'
   }

   const handleStartStop = () => dispatch(togglePlayPause())

   const handleResetButton = () => {
      dispatch(resetCounter())

      if (audioFile && audioFile.current) {
         audioFile.current.pause()
         audioFile.current.currentTime = 0
      }
   }

   const timer = useRef<NodeJS.Timeout | null>(null)
   useEffect(() => {
      const clearTimer = () => {
         if (timer.current) {
            clearInterval(timer.current)
            timer.current = null
         }
      }

      if (isPlaying && !timer.current) {
         timer.current = setInterval(() => dispatch(updateTimer()), 1000)
      } else clearTimer()

      return () => clearTimer()
      // eslint-disable-next-line react-hooks/exhaustive-deps
   }, [isPlaying])

   useEffect(() => {
      if (timeRemaining === 0) {
         if (timerType === BREAK && audioFile && audioFile.current) {
            audioFile.current.play()
         }
      }
   })

   const handleIncrement = (type: COUNTER_TYPE) => () => dispatch(increment(type))
   const handleDecrement = (type: COUNTER_TYPE) => () => dispatch(decrement(type))

   return (
      <main className="app-container">
         <div className="pomodoro-timer">
            <div className="title">Pomodoro Clock</div>
            <div className="labels-wrapper">
               <TimerInput
                  className="label__break"
                  id="break-label"
                  downArrowId="break-decrement"
                  upArrowId="break-increment"
                  lengthId="break-length"
                  title="Break Length"
                  value={breakLength}
                  increment={isPlaying ? noop : handleIncrement(BREAK)}
                  decrement={isPlaying ? noop : handleDecrement(BREAK)}
               />
               <TimerInput
                  className="label__session"
                  id="session-label"
                  downArrowId="session-decrement"
                  upArrowId="session-increment"
                  lengthId="session-length"
                  title="Session Length"
                  value={sessionLength}
                  increment={isPlaying ? noop : handleIncrement(SESSION)}
                  decrement={isPlaying ? noop : handleDecrement(SESSION)}
               />
            </div>
            <div className="timer-wrapper">
               <Circle
                  percent={percentageCompleted}
                  strokeWidth={4}
                  trailColor="#bacca4"
                  strokeColor={timerColor}
                  className="timer__progress-bar"
               />
               <div className="timer__info-wrapper">
                  <div className="label__title" id="timer-label">
                     {timerType}
                  </div>
                  <div className="timer__countdown" id="time-left">
                     {counterTime}
                  </div>
                  <div className="controls-wrapper">
                     {isPlaying ? (
                        <button
                           className="controls__play-pause"
                           id="start_stop"
                           onClick={handleStartStop}
                        >
                           <PauseButton />
                        </button>
                     ) : (
                        <button
                           className="controls__play-pause"
                           id="start_stop"
                           onClick={handleStartStop}
                        >
                           <PlayButton />
                        </button>
                     )}
                     <button
                        className="controls__reset"
                        id="reset"
                        onClick={handleResetButton}
                     >
                        <ResetButton />
                     </button>
                  </div>
               </div>
            </div>
         </div>
         <audio id="beep" src={require('../audio/BeepSound.wav')} ref={audioFile}>
            Your browser does not support the
            <code>audio</code> element.
         </audio>
      </main>
   )
}

export { App }
