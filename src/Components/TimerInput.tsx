import React from 'react'

import { ReactComponent as DownArrow } from '../icons/arrow-down-solid.svg'
import { ReactComponent as UpArrow } from '../icons/arrow-up-solid.svg'

type TimerInputProps = {
   title: string
   value: number
   className: string
   id: string
   downArrowId: string
   upArrowId: string
   lengthId: string
   increment: () => void
   decrement: () => void
}

const TimerInput: React.FC<TimerInputProps> = ({
   title,
   value,
   id,
   className,
   downArrowId,
   upArrowId,
   lengthId,
   increment,
   decrement
}) => {
   return (
      <div className={className}>
         <div id={id} className="label__title">
            {title}
         </div>
         <div className="label__controls">
            <button
               className="label__controls__arrow"
               id={downArrowId}
               onClick={decrement}
            >
               <DownArrow />
            </button>
            <div className="label__controls__value" id={lengthId}>
               {value}
            </div>
            <button className="label__controls__arrow" id={upArrowId} onClick={increment}>
               <UpArrow />
            </button>
         </div>
      </div>
   )
}

export { TimerInput }
