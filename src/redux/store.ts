import { applyMiddleware, compose, createStore } from 'redux'
import thunk from 'redux-thunk'

import { reducer } from './breakReducer'

const rootReducer = reducer

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

const middlewares: any[] = [thunk]

const enhancer = composeEnhancers(applyMiddleware(...middlewares))

const store = createStore(rootReducer, undefined, enhancer)

export type Store = ReturnType<typeof rootReducer>

export { store }
