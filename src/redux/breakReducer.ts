import { Dispatch } from 'react'

import {
   calculatePercentagePassed,
   clampToPositive,
   minutesToMilliseconds
} from '../utils'

type InitialState = {
   [key: string]: string | number | boolean
   breakLength: number
   sessionLength: number
   timeRemaining: number
   timerType: string
   isPlaying: boolean
}

const BREAK = 'Break'
const SESSION = 'Session'

const initialState = {
   timeRemaining: minutesToMilliseconds(25),
   timerType: SESSION,
   breakLength: 5,
   sessionLength: 25,
   isPlaying: false
}

/**
 *
 * selectors
 */
const getBreakLength = (state: InitialState) => state.breakLength
const getSessionLength = (state: InitialState) => state.sessionLength
const getTimeRemaining = (state: InitialState) => state.timeRemaining
const getTimerType = (state: InitialState) => state.timerType
const getIsPlaying = (state: InitialState) => state.isPlaying

const getPercentageCompleted = (state: InitialState) => {
   const timerType = getTimerType(state)
   const timeRemaining = getTimeRemaining(state)
   const sessionLength = minutesToMilliseconds(getSessionLength(state))
   const breakLength = minutesToMilliseconds(getBreakLength(state))

   const percentage =
      timerType === SESSION
         ? // subtract from 100% to animate clockwise
           100 - calculatePercentagePassed(sessionLength, timeRemaining)
         : calculatePercentagePassed(breakLength, timeRemaining)

   return clampToPositive(percentage)
}

/**
 *
 * actions
 */
const INCREMENT_BREAK = 'INCREMENT_BREAK'
const INCREMENT_SESSION = 'INCREMENT_SESSION'
const DECREMENT_BREAK = 'DECREMENT_BREAK'
const DECREMENT_SESSION = 'DECREMENT_SESSION'
const RESET_COUNTER = 'RESET_COUNTER'
const UPDATE_TIMER = 'UPDATE_TIMER'
const TOGGLE_TIMER_TYPE = 'TOGGLE_TIMER_TYPE'
const TOGGLE_IS_PLAYING = 'TOGGLE_IS_PLAYING'

type SESSION = typeof SESSION
type BREAK = typeof BREAK
export type COUNTER_TYPE = SESSION | BREAK

/**
 *
 *
 */
const updateTimer = () => {
   return (dispatch: Dispatch<any>, getState: () => InitialState) => {
      const state = getState()
      const prevTime = getTimeRemaining(state)
      const isStillRunning = prevTime - 1000 >= 0
      const isSession = getTimerType(state) === SESSION
      const breakLength = getBreakLength(state)
      const sessionLength = getSessionLength(state)

      if (prevTime - 1000 === 0) {
         dispatch({
            type: TOGGLE_TIMER_TYPE,
            payload: isSession ? BREAK : SESSION
         })
      }

      if (isStillRunning) {
         dispatch({
            type: UPDATE_TIMER,
            payload: prevTime - 1000
         })
      } else {
         dispatch({
            type: UPDATE_TIMER,
            // we have to use the currently set session value since
            // it has already been changed when the timer hit zero
            payload: isSession
               ? minutesToMilliseconds(sessionLength)
               : minutesToMilliseconds(breakLength)
         })
      }
   }
}

/**
 *
 *
 */
const increment = (type: COUNTER_TYPE) => {
   return (dispatch: Dispatch<any>, getState: any) => {
      const state = getState()
      const isSession = getTimerType(state) === SESSION
      const prevLength =
         type === SESSION ? getSessionLength(state) : getBreakLength(state)

      if (prevLength === 60) return

      if (prevLength === 60) return

      dispatch({
         type: type === BREAK ? INCREMENT_BREAK : INCREMENT_SESSION,
         payload: prevLength + 1,
         meta: {
            isSession
         }
      })
   }
}

/**
 *
 *
 */
const decrement = (type: COUNTER_TYPE) => {
   return (dispatch: Dispatch<any>, getState: () => InitialState) => {
      const state = getState()
      const isSession = getState().timerType === SESSION
      const prevLength =
         type === SESSION ? getSessionLength(state) : getBreakLength(state)

      if (prevLength === 1) return

      if (prevLength === 1) return

      dispatch({
         type: type === BREAK ? DECREMENT_BREAK : DECREMENT_SESSION,
         payload: prevLength - 1,
         meta: {
            isSession
         }
      })
   }
}

/**
 *
 *
 */
const togglePlayPause = () => {
   return (dispatch: Dispatch<any>, getState: () => InitialState) => {
      const isPlaying = getIsPlaying(getState())

      dispatch({
         type: TOGGLE_IS_PLAYING,
         payload: !isPlaying
      })
   }
}

/**
 *
 *
 */
const resetCounter = () => ({
   type: RESET_COUNTER
})

/**
 *
 */
const reducer = (state = initialState, action: any) => {
   switch (action.type) {
      case INCREMENT_BREAK:
      case DECREMENT_BREAK:
         if (!action.meta.isSession) {
            return {
               ...state,
               breakLength: action.payload,
               timeRemaining: minutesToMilliseconds(action.payload)
            }
         } else {
            return {
               ...state,
               breakLength: action.payload
            }
         }

      case INCREMENT_SESSION:
      case DECREMENT_SESSION:
         if (action.meta.isSession) {
            return {
               ...state,
               sessionLength: action.payload,
               timeRemaining: minutesToMilliseconds(action.payload)
            }
         } else {
            return {
               ...state,
               sessionLength: action.payload
            }
         }

      case RESET_COUNTER:
         return initialState

      case UPDATE_TIMER:
         return { ...state, timeRemaining: action.payload }

      case TOGGLE_TIMER_TYPE:
         return {
            ...state,
            timerType: action.payload
         }

      case TOGGLE_IS_PLAYING:
         return {
            ...state,
            isPlaying: action.payload
         }
      default:
         return state
   }
}

export {
   BREAK,
   decrement,
   getBreakLength,
   getIsPlaying,
   getPercentageCompleted,
   getSessionLength,
   getTimeRemaining,
   getTimerType,
   increment,
   reducer,
   resetCounter,
   SESSION,
   togglePlayPause,
   updateTimer
}
