/**
 *
 * clamp provided number to 0 or higher
 */
const clampToPositive = (n: number) => (n < 0 ? 0 : n)

/**
 * copy-pasta https://stackoverflow.com/a/21294619
 *
 * convert milliseconds to mm : ss
 */
const millisToMinutesAndSeconds = (millis: number) => {
   const minutes = clampToPositive(Math.floor(millis / 60000))
   const seconds = clampToPositive((millis % 60000) / 1000).toFixed(0)

   return `${minutes >= 10 ? minutes : `0${minutes}`}:${
      Number(seconds) >= 10 ? seconds : `0${seconds}`
   }`
}

/**
 *
 * convert minutes to milliseconds
 */
const minutesToMilliseconds = (minutes: number) => minutes * 60000

/**
 *
 * no operation
 */
const noop = () => {}

/**
 *
 * calculate the percentage of remaining time
 */
const calculatePercentagePassed = (starting: number, remaining: number) =>
   (remaining / starting) * 100

export {
   calculatePercentagePassed,
   millisToMinutesAndSeconds,
   minutesToMilliseconds,
   clampToPositive,
   noop
}
