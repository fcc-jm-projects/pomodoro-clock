declare global {
   interface Window {
      __REDUX_DEVTOOLS_EXTENSION__: () => {}
      __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: (x: any) => any
   }
}

export {}